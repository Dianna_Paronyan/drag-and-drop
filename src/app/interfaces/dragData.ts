export interface DragData {
  draggingItemIndex: number;
  draggingListIndex: number;
  listItemIndex:number,
  listIndex:number
}
