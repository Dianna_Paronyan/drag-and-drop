import {Directive, HostBinding, HostListener, Input} from '@angular/core';
import {DragData} from "../interfaces/dragData";
import {DataService} from "../services/data.service";

@Directive({
  selector: '[appDragDrop]'
})
export class DragDropDirective {

  @Input() draggingItemIndex: number | undefined;
  @Input() draggingListIndex: number | undefined;
  @Input() listItemIndex: number | undefined;
  @Input() listIndex: number | undefined;
  @HostBinding('class.drag-over') isDragOver: boolean = false;

  constructor(private dService: DataService) {}

  @HostListener('dragstart', ['$event'])
  onDragStart() {
    if (
      this.draggingItemIndex !== undefined &&
      this.draggingListIndex !== undefined &&
      this.listItemIndex !== undefined &&
      this.listIndex !== undefined
    ) {
      this.dService.setDragData({
        draggingItemIndex: this.draggingItemIndex,
        draggingListIndex: this.draggingListIndex,
        listItemIndex: this.listItemIndex,
        listIndex: this.listIndex
      });
    }
  }
  @HostListener('dragover', ['$event'])
  onDragOver(event: DragEvent) {
    event.preventDefault();
    this.isDragOver = true;
  }

  @HostListener('dragleave', ['$event'])
  onDragLeave() {
    this.isDragOver = false;
  }

  @HostListener('drop', ['$event'])
  onDrop(event: DragEvent) {
    event.preventDefault();

    const dragData:DragData = this.dService.getDragData();

    if (
      dragData &&
      dragData.draggingItemIndex !== undefined &&
      dragData.draggingListIndex !== undefined &&
      this.listItemIndex !== undefined &&
      this.listIndex !== undefined
    ) {
      this.dService.moveItem(
        dragData.draggingListIndex,
        this.listIndex,
        dragData.draggingItemIndex,
        this.listItemIndex
      );
    }

    this.isDragOver = false;

  }

}
