import { Injectable } from '@angular/core';
import {DragData} from "../interfaces/dragData";

@Injectable({
  providedIn: 'root'
})
export class DataService {
  dragData: DragData = {} as DragData;

  private list1: string[] = [
    'Elem1_1',
    'Elem1_2',
    'Elem1_3',
    'Elem1_4',
    'Elem1_5',
  ];

  private list2: string[] = [
    'Elem2_1',
    'Elem2_2',
    'Elem2_3',
  ];

  private list3: string[] = [
    'Elem3_1',
    'Elem3_2',
  ];

  constructor() { }

  getLists() {
    return [this.list1, this.list2, this.list3];
  }

  moveItem(listIndex: number, targetListIndex: number, index: number, targetIndex: number) {
    const list: string[] = this.getListByIndex(listIndex);

    const draggedItem = list.splice(index, 1)[0];
    this.getListByIndex(targetListIndex).splice(targetIndex, 0, draggedItem);
  }

  private getListByIndex(index: number) {
    return index === 1 ? this.list1 : index === 2 ? this.list2 : this.list3;
  }

  setDragData(data:DragData) {
    this.dragData = data;
  }

  getDragData(): DragData {
    return this.dragData;
  }
}
